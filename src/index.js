"use strict"
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRedirect, IndexRoute, hashHistory } from 'react-router';
import { Language, Privileges } from 'Utils';
//导航组件
import App from './app';
import './style';
import './images/favicon.ico';
import 'babel-polyfill';
class Index extends Component {
    render() {
        const i18n = Language.getLanguage('index');
        return (
            <Router history={hashHistory}>
                <Route path="/login" breadcrumbName={i18n.login}>
                    <IndexRoute getComponent={function (nexState, callback) {
                       import('./controllers/login').then(m => {
                        callback(null, m['Login']);
                    })
                     }
                   }/>
                </Route>
                <Route path="/" breadcrumbName={i18n.home} component={App}>
                    <IndexRoute getComponent={function (nexState, callback) {
                        import('./controllers/homePage').then(m => {
                        callback(null, m['HomePage']);
                    })
                            }
                        }/>
                    {/*<Route path="project" breadcrumbName={'项目中心'}>
                        <Route path="ProjectManage" breadcrumbName={'项目管理'} getComponent={function (nexState, callback) {
                             import('./controllers/project').then(m => {
                            callback(null, m['ProjectManage']);
                        })
                         }}/>
                         <Route path="TenantManage" breadcrumbName={'租户管理'} getComponent={function (nexState, callback) {
                             import('./controllers/project').then(m => {
                            callback(null, m['TenantManage']);
                        })
                         }}/>
                         <IndexRedirect to="ProjectInfo" />
                    </Route>*/}
                </Route>
            </Router>
        )
    }
}

ReactDOM.render(<Index />, document.getElementById('root'));
