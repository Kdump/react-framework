"use strict"
import React, { Component } from 'react';
import { Layout, Icon, Menu, Breadcrumb, Select, Button, Popconfirm, message, Form, Tooltip } from 'antd';
import { Link, hashHistory } from 'react-router';
import { Language, Fetch, Privileges } from 'Utils';
import { userService, ProjectService, tenantService } from 'Services';
const { Header, Sider, Content, Footer } = Layout;
const { SubMenu, Item: MenuItem, ItemGroup: MenuItemGroup } = Menu;
const FormItem = Form.Item;
//隐藏动画
document.getElementById('particles-js').style.display = 'none';
/**
 * @message {token管理}
 */
// function checkToken() {
//     const access_token = localStorage.getItem('access_token');
//     if (!access_token) {
//         let key = window.location.hash;
//         if (key.indexOf('/login') == -1) {
//             const hash = hashHistory.getCurrentLocation().pathname;
//             const LOGIN_HASH = localStorage.getItem('LOGIN_HASH');
//             if (!LOGIN_HASH) {
//                 let hashs = hash.slice(1, hash.length);
//                 localStorage.setItem('LOGIN_HASH', hashs);
//             }
//             hashHistory.push('/login');
//         }
//     } else {
//         Fetch.initTokens();
//     }
// }

// checkToken();

/**
 * @message {用户基本信息}
 */
let dataUser = localStorage.getItem('user_claims');
try {
    dataUser = JSON.parse(dataUser);
} catch (e) {
    console.log(e);
}

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            curLocale: Language.getCurLocale(),//获取语言
            leftShrink: false,//左侧菜单缩小
        };
    }
    componentDidMount() {
        //隐藏背景图
        document.getElementById('particles-js').style.display = 'none';
    }
    //返回上一步
    backup() {
        history.go(-1);
    }
    //缩小放大操作
    shrink(val) {
        this.setState({ leftShrink: !val });
    }
    //切换语言风格
    onChangeLanguage = (value) => {
        const { curLocale } = this.state;
        localStorage.setItem('LANGUAGE_STYLE', value);
        this.setState({ curLocale: value });
        location.reload();
    }
    //登出
    onhandleLoginOut = () => {
        hashHistory.push('/login');
        // let data = {
        //     utoken: localStorage.getItem('access_token')
        // }
        // userService.outLogin(data, (res) => {
        //     if (res.sys.status == 'S') {
        //         hashHistory.push('/login');
        //         localStorage.clear();
        //     } else {
        //         message.error(res.sys.erortx);
        //     }
        // });
    }
    render() {
        const { curLocale, leftShrink} = this.state;
        const i18n = Language.getLanguage('index');
        const { routes, params, children } = this.props;
        const paths = routes.map(item => item.path).filter(item => item);
        paths.length > 1 && paths.shift(); // 如果 paths 长度大于0 那么进入子路由。 / , RPC , ServiceList . 此时得把 / 去掉，使得首页Item不再高亮
        return (
            <Layout style={{ minHeight: "100%" }}>
                <Header id="header">
                    <div className="logo"></div>
                    <div className="header-menu">
                        <i className="icon-paragraph-justify3" onClick={this.shrink.bind(this, leftShrink)}></i>
                        <div className="login-message fr">
                            <i className="icon-users">
                                <span className="user-span">{dataUser && dataUser.relnam ? dataUser.relnam : ''}</span>
                            </i>
                            <Popconfirm placement="topRight" title={i18n.loginOutMessage}
                                onConfirm={this.onhandleLoginOut} okText={i18n.ok} cancelText={i18n.cancel}>
                                <i className="icon-switch2"></i>
                            </Popconfirm>
                        </div>
                        <div className="language-message fr">
                            <Select className="selectLan" defaultValue={curLocale} onSelect={this.onChangeLanguage}>
                                <Option value="zh"><a className="language"><lable className="lableLan cn-language"></lable><span>{i18n.cn}</span></a></Option>
                                <Option value="en"><a className="language"><lable className="lableLan us-language"></lable><span>{i18n.us}</span></a></Option>
                            </Select>
                        </div>
                    </div>
                </Header>
                <Layout>
                    <Sider width={leftShrink ? 0 : 200} id="slider">
                        <div className={'slider-title'}>{i18n.platform}</div>
                        <Menu theme="dark" mode="inline" selectedKeys={paths} defaultOpenKeys={paths}>
                            <MenuItem key="/">
                                <Link to="/" onlyActiveOnIndex={true}><i className="icon-home4" /><span>{i18n.home}</span></Link>
                            </MenuItem>
                            {/*<SubMenu key="project" title={<span><i className="icon-pie-chart4" /><span>项目中心</span></span>}>
                                <MenuItem key="ProjectManage">
                                    <Link to="/project/ProjectManage">项目管理</Link>
                                </MenuItem>
                                <MenuItem key="TenantManage">
                                    <Link to="/project/TenantManage">租户管理</Link>
                                </MenuItem>
                            </SubMenu>*/}
                        </Menu>
                        <div style={{ marginBottom: 150 }} />
                    </Sider>
                    <Layout id="main" style={{ marginLeft: (leftShrink ? '0' : '200') + 'px' }}>
                        <Content id="content">
                            <div className="content-header">
                                <Breadcrumb routes={routes.filter(item => item.path && item)} params={params} separator=">" />
                                <Button className="backup" onClick={this.backup.bind(this)}><i className="icon-arrow-left22"></i><span>{i18n.backUp}</span></Button>
                            </div>
                            <div className="main-content">
                                {children}
                            </div>
                        </Content>
                        <Footer id="footer" className="tr"><span>{i18n.version}</span><a href="http://www.sunline.cn/" target="_blank"><span>{i18n.companyName}</span></a></Footer>
                    </Layout>
                </Layout>
            </Layout>
        );
    }
}
